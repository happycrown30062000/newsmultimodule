package com.nsu.core

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
@Target(allowedTargets = [AnnotationTarget.FUNCTION, AnnotationTarget.CLASS])
annotation class FeatureScope
