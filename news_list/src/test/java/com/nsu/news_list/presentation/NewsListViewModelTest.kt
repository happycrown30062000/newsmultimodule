package com.nsu.news_list.presentation

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.nsu.api.domain.model.News
import com.nsu.news_list.TestData
import com.nsu.news_list.domain.useCase.GetTopHeadlinesUseCase
import com.nsu.news_list.presentation.navigation.NewsListNavigationProvider
import io.mockk.*
import kotlinx.coroutines.*
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import kotlin.properties.Delegates

@ExperimentalCoroutinesApi
@DelicateCoroutinesApi
class NewsListViewModelTest {

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    private var testDispatcher: ExecutorCoroutineDispatcher by Delegates.notNull()

    private val getTopHeadlinesUseCase: GetTopHeadlinesUseCase = mockk {
        coEvery { this@mockk.invoke() } returns TestData.newsList
    }

    private val newsListNavigationProvider: NewsListNavigationProvider = mockk {
        justRun { toNewsDetailsScreen(TestData.NEWS) }
    }

    private val newsObserver: Observer<List<News>> = mockk(relaxed = true)

    @Before
    fun setUp() {
        testDispatcher = newSingleThreadContext("Context")
        Dispatchers.setMain(dispatcher = testDispatcher)

    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.close()
    }

    @Test
    fun `Create viewModel EXPECT correct list loaded`() = runTest {
        val viewModel = NewsListViewModel(
            getTopHeadlinesUseCase = getTopHeadlinesUseCase,
            newsListNavigationProvider = newsListNavigationProvider
        )
        viewModel.news.observeForever(newsObserver)

        coVerify { getTopHeadlinesUseCase() }
        verify { newsObserver.onChanged(TestData.newsList) }

    }

    @Test
    fun `Call next button clicked EXPECT navigation called`() {
        val viewModel = NewsListViewModel(
            getTopHeadlinesUseCase = getTopHeadlinesUseCase,
            newsListNavigationProvider = newsListNavigationProvider
        )

        viewModel.onNewsItemClicked(news = TestData.NEWS)

        verify { newsListNavigationProvider.toNewsDetailsScreen(news = TestData.NEWS) }
    }

}