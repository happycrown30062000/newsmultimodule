package com.nsu.news_list.domain.useCase

import com.google.common.truth.Truth.assertThat
import com.nsu.api.domain.repository.NewsRepository
import com.nsu.news_list.TestData
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

@ExperimentalCoroutinesApi
class GetTopHeadlinesUseCaseTest {

    private val newsRepository: NewsRepository = mockk {
        coEvery { getTopHeadlines() } returns TestData.newsList
    }

    @Test
    fun `Get top headlines EXPECT correct list`() = runTest {
        val getTopHeadlinesUseCase = GetTopHeadlinesUseCase(
            newsRepository = newsRepository
        )

        val actual = getTopHeadlinesUseCase()

        val expected = TestData.newsList
        assertThat(actual).isEqualTo(expected)
    }

}