package com.nsu.news_list

import com.nsu.api.domain.model.News

object TestData {

    private const val ITEMS_IN_LIST = 3
    private const val TITLE = "title"
    private const val DESCRIPTION = "description"
    private const val PUBLISH_DATE = "16-04-2022"
    private const val IMAGE_URL = "image_url"

    val NEWS = News(
        title = TITLE,
        description = DESCRIPTION,
        publishDate = PUBLISH_DATE,
        imageUrl = IMAGE_URL
    )

    val newsList = buildList<News> {
        repeat(ITEMS_IN_LIST) {
            NEWS
        }
    }


}