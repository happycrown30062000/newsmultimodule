package com.nsu.news_list.di

import com.nsu.core.FeatureScope
import com.nsu.news_list.presentation.NewsListFragment
import dagger.Component
import kotlin.properties.Delegates

@FeatureScope
@Component(dependencies = [NewsListDependencies::class])
internal interface NewsListComponent {

    fun inject(newsListFragment: NewsListFragment)

    @Component.Factory
    interface Factory {

        fun create(dependencies: NewsListDependencies): NewsListComponent

    }

}

internal interface NewsListDependenciesProvider {

    var newsListDependencies: NewsListDependencies

    companion object : NewsListDependenciesProvider by NewsListDependenciesStore
}

object NewsListDependenciesStore : NewsListDependenciesProvider {

    override var newsListDependencies: NewsListDependencies by Delegates.notNull()
}