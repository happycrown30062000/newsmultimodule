package com.nsu.news_list.di

import com.nsu.api.domain.repository.NewsRepository
import com.nsu.news_list.presentation.navigation.NewsListNavigationProvider

interface NewsListDependencies {

    val newsRepository: NewsRepository
    val newsListNavigationProvider: NewsListNavigationProvider

}