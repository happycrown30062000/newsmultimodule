package com.nsu.news_list.presentation

import androidx.lifecycle.ViewModel
import com.nsu.news_list.di.DaggerNewsListComponent
import com.nsu.news_list.di.NewsListDependenciesProvider

internal class NewsListComponentViewModel : ViewModel() {

    val newsListComponent = DaggerNewsListComponent.factory().create(
        dependencies = NewsListDependenciesProvider.newsListDependencies
    )

}