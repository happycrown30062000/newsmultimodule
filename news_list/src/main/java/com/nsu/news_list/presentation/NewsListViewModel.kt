package com.nsu.news_list.presentation

import androidx.lifecycle.*
import com.nsu.api.domain.model.News
import com.nsu.news_list.domain.useCase.GetTopHeadlinesUseCase
import com.nsu.news_list.presentation.navigation.NewsListNavigationProvider
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Provider

internal class NewsListViewModel(
    private val getTopHeadlinesUseCase: GetTopHeadlinesUseCase,
    private val newsListNavigationProvider: NewsListNavigationProvider
) : ViewModel() {

    private val _news = MutableLiveData<List<News>>()
    val news: LiveData<List<News>> = _news

    init {
        viewModelScope.launch {
            val news = getTopHeadlinesUseCase()
            _news.value = news
        }
    }

    fun onNewsItemClicked(news: News) =
        newsListNavigationProvider.toNewsDetailsScreen(news = news)

    @Suppress("UNCHECKED_CAST")
    class Factory @Inject constructor(
        private val getTopHeadlinesUseCaseProvider: Provider<GetTopHeadlinesUseCase>,
        private val newsListNavigationProviderProvider: Provider<NewsListNavigationProvider>
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            require(modelClass == NewsListViewModel::class.java)
            return NewsListViewModel(
                getTopHeadlinesUseCase = getTopHeadlinesUseCaseProvider.get(),
                newsListNavigationProvider = newsListNavigationProviderProvider.get()
            ) as T
        }
    }

}