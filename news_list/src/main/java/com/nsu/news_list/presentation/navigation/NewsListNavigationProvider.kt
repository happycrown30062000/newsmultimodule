package com.nsu.news_list.presentation.navigation

import com.nsu.api.domain.model.News

interface NewsListNavigationProvider {

    fun toNewsDetailsScreen(news: News)

}