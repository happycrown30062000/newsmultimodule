package com.nsu.news_list.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.nsu.api.domain.model.News
import com.nsu.news_list.databinding.FragmentNewsListBinding
import dagger.Lazy
import javax.inject.Inject

class NewsListFragment : Fragment() {

    @Inject
    internal lateinit var newsListViewModelFactory: Lazy<NewsListViewModel.Factory>

    private val viewModel: NewsListViewModel by viewModels(factoryProducer = {
        newsListViewModelFactory.get()
    })

    private var _binding: FragmentNewsListBinding? = null
    private val binding by lazy {
        _binding!!
    }

    override fun onAttach(context: Context) {
        ViewModelProvider(this)
            .get(NewsListComponentViewModel::class.java)
            .newsListComponent
            .inject(newsListFragment = this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentNewsListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            nextButton.setOnClickListener {
                viewModel.onNewsItemClicked(
                    news = News(
                        title = "Very interesting news",
                        description = "Some text for description",
                        imageUrl = "",
                        publishDate = ""
                    )
                )
            }
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}