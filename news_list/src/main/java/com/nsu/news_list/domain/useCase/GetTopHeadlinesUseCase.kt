package com.nsu.news_list.domain.useCase

import com.nsu.api.domain.model.News
import com.nsu.api.domain.repository.NewsRepository
import com.nsu.core.FeatureScope
import javax.inject.Inject

@FeatureScope
class GetTopHeadlinesUseCase @Inject constructor(
    private val newsRepository: NewsRepository
) {

    suspend operator fun invoke(): List<News> =
        newsRepository.getTopHeadlines()

}