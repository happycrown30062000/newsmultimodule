package com.nsu.newsmultimodule

import android.app.Application
import com.nsu.news_details.di.NewsDetailsDependenciesStore
import com.nsu.news_list.di.NewsListDependenciesStore
import com.nsu.newsmultimodule.di.DaggerAppComponent

class NewsApplication : Application() {

    val appComponent by lazy {
        DaggerAppComponent.factory().create()
    }

    override fun onCreate() {
        super.onCreate()
        NewsListDependenciesStore.newsListDependencies = appComponent
        NewsDetailsDependenciesStore.newsDetailsDependencies = appComponent
    }

}