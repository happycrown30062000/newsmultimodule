package com.nsu.newsmultimodule.presentation.naviagtion

import com.github.terrakok.cicerone.androidx.FragmentScreen
import com.nsu.api.domain.model.News
import com.nsu.news_details.presentation.NewsDetailsFragment

object Screens {

    fun NewsDetailsScreen(news: News) = FragmentScreen { NewsDetailsFragment.newInstance(news = news) }
}