package com.nsu.newsmultimodule.presentation.naviagtion

import com.github.terrakok.cicerone.Router
import com.nsu.api.domain.model.News
import com.nsu.news_list.presentation.navigation.NewsListNavigationProvider
import javax.inject.Inject

class NewsListNavigationProviderImpl @Inject constructor(
    private val router: Router
) : NewsListNavigationProvider {

    override fun toNewsDetailsScreen(news: News) =
        router.navigateTo(Screens.NewsDetailsScreen(news = news))

}