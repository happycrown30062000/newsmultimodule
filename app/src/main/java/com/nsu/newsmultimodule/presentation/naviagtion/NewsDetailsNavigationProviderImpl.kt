package com.nsu.newsmultimodule.presentation.naviagtion

import com.github.terrakok.cicerone.Router
import com.nsu.news_details.presentation.navigation.NewsDetailsNavigationProvider
import javax.inject.Inject

class NewsDetailsNavigationProviderImpl @Inject constructor(
    private val router: Router
) : NewsDetailsNavigationProvider {

    override fun toNewsListScreen() =
        router.exit()
}