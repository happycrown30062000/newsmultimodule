package com.nsu.newsmultimodule.di

import javax.inject.Scope

@Scope
@Retention(value = AnnotationRetention.RUNTIME)
@Target(allowedTargets = [AnnotationTarget.FUNCTION, AnnotationTarget.CLASS])
annotation class AppScope