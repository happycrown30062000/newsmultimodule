package com.nsu.newsmultimodule.di.module

import com.nsu.api.data.api.NewsApi
import com.nsu.api.data.dataSource.NewRemoteDataSource
import com.nsu.api.data.dataSource.NewsDataSource
import com.nsu.api.data.repository.NewsRepositoryImpl
import com.nsu.api.di.NewsApiHolder
import com.nsu.api.domain.repository.NewsRepository
import com.nsu.newsmultimodule.di.AppScope
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface ApiModule {

    companion object {
        @AppScope
        @Provides
        fun provideNewsApi(): NewsApi = NewsApiHolder.createNewsApi()
    }

    @AppScope
    @Binds
    fun bindNewsRemoteDataSourceToNewsDataSource(newsRemoteDataSource: NewRemoteDataSource): NewsDataSource

    @AppScope
    @Binds
    fun bindNewsRepositoryImplToNewsRepository(newsRepositoryImpl: NewsRepositoryImpl): NewsRepository

}