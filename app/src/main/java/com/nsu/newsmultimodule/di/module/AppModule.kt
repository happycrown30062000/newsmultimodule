package com.nsu.newsmultimodule.di.module

import dagger.Module

@Module(includes = [ApiModule::class, NavigationModule::class])
interface AppModule