package com.nsu.newsmultimodule.di.module

import com.github.terrakok.cicerone.Cicerone
import com.github.terrakok.cicerone.NavigatorHolder
import com.github.terrakok.cicerone.Router
import com.nsu.news_details.presentation.navigation.NewsDetailsNavigationProvider
import com.nsu.news_list.presentation.navigation.NewsListNavigationProvider
import com.nsu.newsmultimodule.di.AppScope
import com.nsu.newsmultimodule.presentation.naviagtion.NewsDetailsNavigationProviderImpl
import com.nsu.newsmultimodule.presentation.naviagtion.NewsListNavigationProviderImpl
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
interface NavigationModule {

    companion object {

        @AppScope
        @Provides
        fun provideCicerone(): Cicerone<Router> =
            Cicerone.create()

        @AppScope
        @Provides
        fun provideNavigatorHolder(cicerone: Cicerone<Router>): NavigatorHolder =
            cicerone.getNavigatorHolder()

        @AppScope
        @Provides
        fun provideRouter(cicerone: Cicerone<Router>): Router =
            cicerone.router

    }

    @AppScope
    @Binds
    fun bindNewsListNavigationProviderImplToNewsListNavigationProvider(
        newsListNavigationProviderImpl: NewsListNavigationProviderImpl
    ): NewsListNavigationProvider

    @AppScope
    @Binds
    fun bindNewsDetailsNavigationProviderImplToNewsDetailsNavigationProvider(
        newsDetailsNavigationProviderImpl: NewsDetailsNavigationProviderImpl
    ): NewsDetailsNavigationProvider

}