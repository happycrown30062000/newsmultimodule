package com.nsu.newsmultimodule.di

import android.content.Context
import com.nsu.news_details.di.NewsDetailsDependencies
import com.nsu.news_list.di.NewsListDependencies
import com.nsu.newsmultimodule.MainActivity
import com.nsu.newsmultimodule.NewsApplication
import com.nsu.newsmultimodule.di.module.AppModule
import dagger.Component

@AppScope
@Component(modules = [AppModule::class])
interface AppComponent : NewsListDependencies, NewsDetailsDependencies {

    fun inject(activity: MainActivity)

    @Component.Factory
    interface Factory {

        fun create(): AppComponent

    }
}

val Context.appComponent: AppComponent
    get() = if (this is NewsApplication) appComponent else applicationContext.appComponent
