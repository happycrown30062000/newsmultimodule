package com.nsu.api.data.mapper

import com.google.common.truth.Truth.assertThat
import com.nsu.api.TestData
import org.junit.Test

class NewsMapperTest {

    @Test
    fun `map to domain EXPECT correct mapping`() {
        val newsEntity = TestData.NEWS_ENTITY
        val mapper = NewsMapper()

        val actual = mapper.mapToDomain(newsEntity = newsEntity)

        val expected = TestData.NEWS
        assertThat(actual).isEqualTo(expected)
    }
}