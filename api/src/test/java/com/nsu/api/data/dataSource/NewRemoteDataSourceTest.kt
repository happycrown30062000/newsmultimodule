package com.nsu.api.data.dataSource

import com.google.common.truth.Truth.assertThat
import com.nsu.api.TestData
import com.nsu.api.data.api.NewsApi
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

@ExperimentalCoroutinesApi
class NewRemoteDataSourceTest {

    private val newsApi: NewsApi = mockk {
        coEvery { getTopHeadlines() } returns TestData.newsEntityList
    }

    @Test
    fun `Get top headlines EXPECT correct list`() = runTest {
        val dataSource = NewRemoteDataSource(newsApi = newsApi)

        val actual = dataSource.getTopHeadlines()

        assertThat(actual).isEqualTo(TestData.newsEntityList)
    }


}