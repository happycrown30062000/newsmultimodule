package com.nsu.api.data.repository

import com.google.common.truth.Truth.assertThat
import com.nsu.api.TestData
import com.nsu.api.data.dataSource.NewsDataSource
import com.nsu.api.data.mapper.NewsMapper
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test

@ExperimentalCoroutinesApi
class NewsRepositoryImplTest {

    private val newsDataSource: NewsDataSource = mockk {
        coEvery { getTopHeadlines() } returns TestData.newsEntityList
    }

    private val mapper: NewsMapper = mockk {
        every { mapToDomain(newsEntity = TestData.NEWS_ENTITY) } returns TestData.NEWS
    }

    @Test
    fun `Get top headlines EXPECT correct list`() = runTest {
        val repository = NewsRepositoryImpl(newsDataSource = newsDataSource, mapper = mapper)

        val actual = repository.getTopHeadlines()

        val expected = TestData.newsList
        assertThat(actual).isEqualTo(expected)
    }
}