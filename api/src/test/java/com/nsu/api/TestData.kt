package com.nsu.api

import com.nsu.api.data.entity.NewsEntity
import com.nsu.api.domain.model.News

object TestData {

    private const val ITEMS_IN_LIST = 3
    private const val TITLE = "title"
    private const val DESCRIPTION = "description"
    private const val PUBLISH_DATE_RAW = "2022-04-16T19:49:37Z"
    private const val PUBLISH_DATE = "16-04-2022"
    private const val IMAGE_URL = "image_url"

    val NEWS_ENTITY = NewsEntity(
        title = TITLE,
        description = DESCRIPTION,
        publishDate = PUBLISH_DATE_RAW,
        imageUrl = IMAGE_URL
    )

    val NEWS = News(
        title = TITLE,
        description = DESCRIPTION,
        publishDate = PUBLISH_DATE,
        imageUrl = IMAGE_URL
    )

    val newsEntityList = buildList<NewsEntity> {
        repeat(ITEMS_IN_LIST) {
            NEWS_ENTITY
        }
    }

    val newsList = buildList<News> {
        repeat(ITEMS_IN_LIST) {
            NEWS
        }
    }


}