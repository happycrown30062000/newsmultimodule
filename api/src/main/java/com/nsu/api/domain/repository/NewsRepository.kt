package com.nsu.api.domain.repository

import com.nsu.api.domain.model.News

interface NewsRepository {

    suspend fun getTopHeadlines(): List<News>

}