package com.nsu.api.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class News(
    val title: String,
    val description: String,
    val imageUrl: String,
    val publishDate: String
) : Parcelable
