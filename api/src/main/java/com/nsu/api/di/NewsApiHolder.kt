package com.nsu.api.di

import com.nsu.api.data.api.NewsApi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create

object NewsApiHolder {

    private const val HEADER_API_KEY = "X-Api-Key"
    private const val BASE_URL = "https://newsapi.org/"
    private const val API_KEY = "9cfbbfd6f4ff47949f9e0ba442402769"

    private fun createOkhttpClient() =
        OkHttpClient.Builder()
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                    .addHeader(HEADER_API_KEY, API_KEY)
                    .build()
                chain.proceed(request = request)
            }
            .build()


    private fun createRetrofit() =
        Retrofit.Builder()
            .client(createOkhttpClient())
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()


    fun createNewsApi(): NewsApi =
        createRetrofit().create()

}