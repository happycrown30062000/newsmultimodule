package com.nsu.api.data.dataSource

import com.nsu.api.data.entity.NewsEntity

interface NewsDataSource {

    suspend fun getTopHeadlines(): List<NewsEntity>

}