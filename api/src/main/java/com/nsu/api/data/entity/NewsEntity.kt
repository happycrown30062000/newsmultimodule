package com.nsu.api.data.entity

import com.squareup.moshi.Json

data class NewsEntity(
    @field:Json(name = "title")
    val title: String,

    @field:Json(name = "description")
    val description: String,

    @field:Json(name = "urlToImage")
    val imageUrl: String,

    @field:Json(name = "publishedAt")
    val publishDate: String
)