package com.nsu.api.data.mapper

import com.nsu.api.data.entity.NewsEntity
import com.nsu.api.domain.model.News
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import javax.inject.Inject

class NewsMapper @Inject constructor() {

    private val datePattern = "dd-MM-yyyy"
    private val dateFormatter = DateTimeFormatter.ofPattern(datePattern)

    fun mapToDomain(newsEntity: NewsEntity): News =
        News(
            title = newsEntity.title,
            description = newsEntity.description,
            imageUrl = newsEntity.imageUrl,
            publishDate = LocalDateTime.ofInstant(
                Instant.parse(newsEntity.publishDate),
                ZoneOffset.UTC
            ).format(dateFormatter)
        )

}