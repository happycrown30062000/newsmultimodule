package com.nsu.api.data.api

import com.nsu.api.data.entity.NewsEntity
import retrofit2.http.GET

interface NewsApi {

    @GET("/v2/top-headlines")
    suspend fun getTopHeadlines(): List<NewsEntity>

}