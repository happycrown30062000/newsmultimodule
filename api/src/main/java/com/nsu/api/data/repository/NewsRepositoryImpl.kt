package com.nsu.api.data.repository

import com.nsu.api.data.dataSource.NewsDataSource
import com.nsu.api.data.mapper.NewsMapper
import com.nsu.api.domain.model.News
import com.nsu.api.domain.repository.NewsRepository
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val newsDataSource: NewsDataSource,
    private val mapper: NewsMapper
) : NewsRepository {

    override suspend fun getTopHeadlines(): List<News> =
        newsDataSource.getTopHeadlines().map { newsEntity ->
            mapper.mapToDomain(newsEntity = newsEntity)
        }
}