package com.nsu.api.data.dataSource

import com.nsu.api.data.api.NewsApi
import com.nsu.api.data.entity.NewsEntity
import javax.inject.Inject

class NewRemoteDataSource @Inject constructor(
    private val newsApi: NewsApi
) : NewsDataSource {

    override suspend fun getTopHeadlines(): List<NewsEntity> =
        newsApi.getTopHeadlines()

}