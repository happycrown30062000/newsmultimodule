package com.nsu.news_details.di

import com.nsu.core.FeatureScope
import com.nsu.news_details.presentation.NewsDetailsFragment
import dagger.Component
import kotlin.properties.Delegates

@FeatureScope
@Component(dependencies = [NewsDetailsDependencies::class])
internal interface NewsDetailsComponent {

    fun inject(fragment: NewsDetailsFragment)

    @Component.Factory
    interface Factory {

        fun create(dependencies: NewsDetailsDependencies): NewsDetailsComponent

    }

}

internal interface NewsDetailsDependenciesProvider {

    var newsDetailsDependencies: NewsDetailsDependencies

    companion object : NewsDetailsDependenciesProvider by NewsDetailsDependenciesStore
}

object NewsDetailsDependenciesStore : NewsDetailsDependenciesProvider {

    override var newsDetailsDependencies: NewsDetailsDependencies by Delegates.notNull()
}