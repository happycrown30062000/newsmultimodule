package com.nsu.news_details.di

import com.nsu.news_details.presentation.navigation.NewsDetailsNavigationProvider

interface NewsDetailsDependencies {

    val newsDetailsNavigationProvider: NewsDetailsNavigationProvider

}