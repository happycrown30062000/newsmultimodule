package com.nsu.news_details.presentation.navigation

interface NewsDetailsNavigationProvider {

    fun toNewsListScreen()

}