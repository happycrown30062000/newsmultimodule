package com.nsu.news_details.presentation

import androidx.lifecycle.ViewModel
import com.nsu.news_details.di.DaggerNewsDetailsComponent
import com.nsu.news_details.di.NewsDetailsDependenciesProvider

internal class NewsDetailsComponentViewModel : ViewModel() {

    val newsDetailsComponent = DaggerNewsDetailsComponent.factory().create(
        dependencies = NewsDetailsDependenciesProvider.newsDetailsDependencies
    )

}