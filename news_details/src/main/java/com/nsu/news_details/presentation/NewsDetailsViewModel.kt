package com.nsu.news_details.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nsu.news_details.presentation.navigation.NewsDetailsNavigationProvider
import javax.inject.Inject
import javax.inject.Provider

class NewsDetailsViewModel(
    private val newsDetailsNavigationProvider: NewsDetailsNavigationProvider
) : ViewModel() {

    fun onBackClicked() =
        newsDetailsNavigationProvider.toNewsListScreen()

    @Suppress("UNCHECKED_CAST")
    class Factory @Inject constructor(
        private val newsDetailsNavigationProviderProvider: Provider<NewsDetailsNavigationProvider>
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            require(modelClass == NewsDetailsViewModel::class.java)
            return NewsDetailsViewModel(
                newsDetailsNavigationProvider = newsDetailsNavigationProviderProvider.get()
            ) as T
        }
    }
}