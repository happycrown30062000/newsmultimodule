package com.nsu.news_details.presentation

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import com.nsu.api.domain.model.News
import com.nsu.news_details.databinding.FragmentNewsDetailsBinding
import dagger.Lazy
import javax.inject.Inject

class NewsDetailsFragment : Fragment() {

    private var _binding: FragmentNewsDetailsBinding? = null
    private val binding by lazy {
        _binding!!
    }

    @Inject
    lateinit var newsDetailsViewModelFactory: Lazy<NewsDetailsViewModel.Factory>

    private val viewModel: NewsDetailsViewModel by viewModels(factoryProducer = {
        newsDetailsViewModelFactory.get()
    })

    companion object {

        private const val NEWS_KEY = "news_key"

        fun newInstance(news: News): NewsDetailsFragment {

            val fragment = NewsDetailsFragment()
            val arguments = bundleOf(
                NEWS_KEY to news
            )
            fragment.arguments = arguments
            return fragment

        }

    }

    override fun onAttach(context: Context) {
        ViewModelProvider(this)
            .get(NewsDetailsComponentViewModel::class.java)
            .newsDetailsComponent
            .inject(fragment = this)
        super.onAttach(context)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentNewsDetailsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        with(binding) {
            val news = arguments?.getParcelable<News>(NEWS_KEY)
            title.text = news?.title
            description.text = news?.description
            backButton.setOnClickListener {
                viewModel.onBackClicked()
            }
        }
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

}