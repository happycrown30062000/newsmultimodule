package com.nsu.news_details.presentation

import com.nsu.news_details.presentation.navigation.NewsDetailsNavigationProvider
import io.mockk.justRun
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class NewsDetailsViewModelTest {

    private val newsDetailsNavigationProvider: NewsDetailsNavigationProvider = mockk {
        justRun { toNewsListScreen() }
    }

    @Test
    fun `Call on back clicked EXPECT called navigation method`() {
        val viewModel = NewsDetailsViewModel(
            newsDetailsNavigationProvider = newsDetailsNavigationProvider
        )

        viewModel.onBackClicked()

        verify { newsDetailsNavigationProvider.toNewsListScreen() }
    }

}